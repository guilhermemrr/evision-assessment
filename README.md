## eVision Assessment
This project was generated with [create-react-app](https://github.com/facebookincubator/create-react-app).

## Project Stack

This project was built with `ReactJS` and `Material UI` libraries.

I chose React because it's a good UI library to work with. I like the JSX syntax that improves the readability and maintainability of the code, and the library has good perfomance to render user interfaces due to the Virtual DOM that it implements. Also, it has a big open source community that is always growing together with it. 

Material UI provides a great set of components that you can just plugin-in in your app and you can start using them. So it's handy for fast prototyping.

The project is also using `moment` but it's just for formatting date purposes.

## Required before start
You should have `npm` or `yarn` installed in your machine to run this project. It's recommend to run with `yarn`.

## Installing dependencies
Run `npm install` or `yarn install` to install dependencies.

## Running the server
You should run `node server.js` inside the `apiserver` folder before start the app. It will start a fake server on http://localhost:8090/

## Running the app
Running `npm start` or `yarn start` will start the project. It will run on http://localhost:3000/.

## Testing
Running `npm test` or `yarn test` will run the unit tests with Jest and Enzyme.
