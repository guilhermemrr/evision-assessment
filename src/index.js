import React from 'react';
import ReactDOM from 'react-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';
import AccountBalance from './containers/AccountBalance.jsx';
import './styles/index.css';

/* Needed for Material-UI library for onTouchTap*/
injectTapEventPlugin();

const App = () => (
  <MuiThemeProvider>
    <AccountBalance />
  </MuiThemeProvider>
);

ReactDOM.render(<App />, document.getElementById('root'));
