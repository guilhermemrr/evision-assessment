import React from 'react';
import ReactDOM from 'react-dom';
import AccountBalance from '../../containers/AccountBalance';
import balanceMock from '../mocks/balance.json';
import { shallow } from 'enzyme';

describe('Component: AccountBalance', () => { 

  it('should render without crash', () => {
    const wrapper = shallow(<AccountBalance />);
    expect(wrapper.state('accountInfo')).toBe(null);
    expect(wrapper.state('error')).toBe(null);
  });

  it('should match with snapshot', () => {
    const wrapper =  shallow(<AccountBalance />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render error message when the server is down',() => {
    const wrapper =  shallow(<AccountBalance />);

    wrapper.instance().showError({ message: '503' });

    expect(wrapper.state('error')).toEqual('503');
    expect(wrapper.find('.error').text()).toEqual('Some error happened with the bank :(');
  });

  it('should render account info and balance', () => {
    const wrapper =  shallow(<AccountBalance />);
    
    wrapper.instance().updateAccountInfo(balanceMock);

    expect(wrapper.find('AccountInfo').props()).toEqual(balanceMock.account);
    expect(wrapper.find('BalanceList').props()).toEqual({ balanceList: balanceMock.debitsAndCredits });
  });

});