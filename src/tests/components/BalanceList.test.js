import React from 'react';
import ReactDOM from 'react-dom';
import BalanceList from '../../components/BalanceList';
import moment from 'moment';
import { shallow } from 'enzyme';

describe('Component: BalanceList', () => { 

  let balanceList;

  beforeEach(() => {
    balanceList =  [{
      'from': 'Wendy',
      'description': 'Diner',
      'amount': 10.50,
      'date': '2016-01-10T09:20:00.000Z'
    }];
  });

  it('should render without crash', () => {
    shallow(<BalanceList balanceList={ balanceList } />)
  });

  it('should match with snapshot', () => {
    const wrapper =  shallow(<BalanceList balanceList={ balanceList } />)
    expect(wrapper).toMatchSnapshot();
  });

  it('should render balance item with correct properties', () => {
    const wrapper =  shallow(<BalanceList balanceList={ balanceList } />)    
    const tableColumns = wrapper.find('TableRowColumn');

    expect(tableColumns.get(0).props.children).toEqual(moment(balanceList[0].date).format('D of MMM'));
    expect(tableColumns.get(1).props.children).toEqual(balanceList[0].description);
    expect(tableColumns.get(2).props.children).toEqual(balanceList[0].to);
    expect(tableColumns.get(3).props.children).toEqual(balanceList[0].from);
    expect(tableColumns.get(4).props.children).toEqual(balanceList[0].amount);
    expect(tableColumns.get(5).props.children).toEqual(balanceList[0].debit);
  });

  it('should render list of items', () => {
    balanceList.push({
      'to': 'Joe\'s Pizza',
      'description': '134678943.88',
      'debit': 31.50,
      'date': '2016-01-09T22:23:00.000Z'
    });

    const wrapper =  shallow(<BalanceList balanceList={ balanceList } />)    
    expect(wrapper.find('.balance-item').length).toEqual(balanceList.length);
  });

});
