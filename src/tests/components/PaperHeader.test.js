import React from 'react';
import ReactDOM from 'react-dom';
import PaperHeader from '../../components/PaperHeader';
import { shallow } from 'enzyme';

describe('Component: PaperHeader', () => { 

  let title;

  beforeEach(() => {
    title = "Overview";
  });

  it('should render without crash', () => {
    shallow(<PaperHeader title={ title } />)
  });

  it('should match with snapshot', () => {
    const wrapper =  shallow(<PaperHeader title={ title } />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render the title', () => {
    const wrapper =  shallow(<PaperHeader title={ title } />);
    expect(wrapper.find('.header-title').text()).toEqual(title);
  });

});
