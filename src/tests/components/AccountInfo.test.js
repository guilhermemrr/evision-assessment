import React from 'react';
import ReactDOM from 'react-dom';
import AccountInfo from '../../components/AccountInfo';
import { shallow } from 'enzyme';

describe('Component: AccountInfo', () => { 

  let accountInfo;

  beforeEach(() => {
    accountInfo = { name: 'James', iban: 'MCNX920S', balance: 5000 };
  });

  it('should render without crash', () => {
    shallow(<AccountInfo { ...accountInfo } />)
  });

  it('should match with snapshot', () => {
    const wrapper =  shallow(<AccountInfo { ...accountInfo } />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render name iban and balance', () => {
    const wrapper =  shallow(<AccountInfo { ...accountInfo } />);
    expect(wrapper.find('#name').text()).toEqual('Name: '+ accountInfo.name);
    expect(wrapper.find('#iban').text()).toEqual('IBAN: '+ accountInfo.iban);
    expect(wrapper.find('#balance').text()).toEqual('Balance: '+ accountInfo.balance);
  });

});
