import React, { Component } from 'react';
import AccountInfo from '../components/AccountInfo';
import BalanceList from '../components/BalanceList';
import '../styles/AccountBalance.css';

class AccountBalance extends Component {

  state = {
    accountInfo: null,
    error: null
  };
 
  componentDidMount = () =>
    fetch('http://localhost:8090/api/getbalance')
      .then((res) => res.json())
      .then(this.updateAccountInfo)
      .catch(this.showError);
      
  updateAccountInfo = (accountInfo) =>
    this.setState({
      error: null,
      accountInfo
    });

  showError = (error) => 
    this.setState({
      error: error.message,
      accountInfo: null,
    });

  render = () => 
    <div className='container'>

      {
        this.state.accountInfo &&
          <div>
            <AccountInfo { ...this.state.accountInfo.account } />
            <BalanceList balanceList={ this.state.accountInfo.debitsAndCredits } />
          </div>
      }

      {
        this.state.error &&
          <div className='error'>Some error happened with the bank :(</div>
      }

    </div>;

}

export default AccountBalance;