import React from 'react';
import PropTypes from 'prop-types';
import Paper from 'material-ui/Paper';
import PaperHeader from './PaperHeader';
import '../styles/AccountInfo.css';

const muiStyle = {
  paper: {
    width: 'auto',
  }
};

const AccountInfo = ({ name, iban, balance }) => (
  <Paper style={ muiStyle.paper }  zDepth={ 1 } >

    <PaperHeader title={ 'Account Info' } />

    <div className='info-container'>
      <div id='name' className='info-text'>Name: { name }</div>
      <div id='iban' className='info-text'>IBAN: { iban }</div>
      <div id='balance' className='info-text'>Balance: { balance }</div>
    </div>

  </Paper>
);

AccountInfo.propTypes = {
  name: PropTypes.string.isRequired,
  iban: PropTypes.string.isRequired,
  balance: PropTypes.number.isRequired,
};

export default AccountInfo;