import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import Paper from 'material-ui/Paper';
import PaperHeader from './PaperHeader';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import '../styles/BalanceList.css';

const BalanceList = ({ balanceList }) => (
  <Paper zDepth={ 1 } >

    <PaperHeader title={ 'Balance Overview' } />

    <Table height={ '300px' } fixedHeader={ true } >
      <TableHeader
        displaySelectAll={ false }
        adjustForCheckbox={ false }
      >
        
        <TableRow>
          <TableHeaderColumn> Date </TableHeaderColumn>
          <TableHeaderColumn> Description </TableHeaderColumn>
          <TableHeaderColumn> To </TableHeaderColumn>
          <TableHeaderColumn> From </TableHeaderColumn>
          <TableHeaderColumn> Money In(€) </TableHeaderColumn>
          <TableHeaderColumn> Money Out(€) </TableHeaderColumn>
        </TableRow>
      </TableHeader>
          
      <TableBody
        displayRowCheckbox={ false }
        showRowHover={ true }
      >

        {
          balanceList.map((item, index) => (
            <TableRow className='balance-item' key={ index }>
              <TableRowColumn>{ moment(item.date).format('D of MMM') }</TableRowColumn>
              <TableRowColumn>{ item.description }</TableRowColumn>
              <TableRowColumn>{ item.to }</TableRowColumn>
              <TableRowColumn>{ item.from }</TableRowColumn>
              <TableRowColumn className='money-in'>{ item.amount }</TableRowColumn>
              <TableRowColumn className='money-out'>{ item.debit }</TableRowColumn>
            </TableRow>
          ))  
        }

      </TableBody>
      
    </Table>

  </Paper>
);

BalanceList.propTypes = {
  balanceList: PropTypes.arrayOf(PropTypes.shape({
    to: PropTypes.string,
    from: PropTypes.string,
    amount: PropTypes.number,
    debit: PropTypes.number,
    date: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
  })).isRequired,
};

export default BalanceList;