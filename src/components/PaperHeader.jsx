import React from 'react';
import PropTypes from 'prop-types';
import '../styles/PaperHeader.css';

const PaperHeader = ({ title }) => (
  <div className='header-container' >
    <div className='header-title'>{ title }</div>
  </div>
);

PaperHeader.propTypes = {
  title: PropTypes.string.isRequired,
};

export default PaperHeader;